package worlds;

import com.haxepunk.World;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.graphics.Image;

import entities.Ship;
import entities.BlackHole;
import entities.Enemy;

class Level extends World
{
	private var blackHole:BlackHole;
	private var mouseDown:Bool = true;
	
	public function new(levelFile:Dynamic = null)
	{
		super();
                
                //Sets up the recycling system for the BlackHole
		blackHole = add(new BlackHole(10, mouseX, mouseY, Image.createRect(10, 10)));
                recycle(blackHole);
		add(new Enemy(HXP.halfWidth, 0, Image.createCircle(10, 0xdeadbeef), null));
	}

	public override function update()
	{
		super.update();		

		if(Input.mouseDown && mouseDown)
		{
			blackHole = add(cast(create(BlackHole), BlackHole));
			blackHole.force = 10;
			blackHole.x = mouseX;
			blackHole.y = mouseY;
                        mouseDown = false;
		}

		if(Input.mouseReleased)
		{
			recycle(blackHole);
			mouseDown = true;
		}
	}
}

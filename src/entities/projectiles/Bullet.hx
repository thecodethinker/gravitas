package entities.projectiles;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Graphics;
import com.haxepunk.Mask;
import com.haxepunk.Vector;

class Bullet extends Entity
{
        private var mDirection:Vector;

        public function new(x:Float, y:Float, graphic:Graphic, mask:Mask, diection:Vector)
        {
                super(x, y, graphic, mask);

                type = "bullet";
        }
}

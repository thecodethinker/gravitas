package entities;

//haxepunk imports
import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.Mask;
import com.haxepunk.Graphic;
import com.haxepunk.math.Vector;
import com.haxepunk.masks.Circle;
import com.haxepunk.utils.Input;

//custom class imports
import entities.BlackHole;

class Ship extends Entity
{
	//variable declarations
        private static var first:Bool = true;
	private var tempTime:Float;
	private var direction:Vector;
	private var dir:Vector;
	private var collideEntity:Entity;
	private var dist:Float;
        private var diameter:Float;
	private var tempHole:BlackHole;
	private var holeExists:Bool;
        private var angle:Float;
        private var radian:Float;
	
	public var speed:Float;

	public function new(x:Float = 0, y:Float = 0, speed:Float = 10, graphic:Graphic = null, mask:Mask = null)
	{
		super(x, y, graphic, mask);
	    	
		//defines some vars
		this.speed = speed;
		holeExists = true;
		
		direction = new Vector(1, 0);

                angle = 0.0;
                radian = angle * (Math.PI / 180);
                diameter = 30;
                dist = 0;

		graphic.x = -10;
		graphic.y = -10;

		setHitbox(50, 50, 25, 25);
	}

	//moves the ship
	private inline function move()
	{
		tempTime = HXP.elapsed;
                
                if(tempHole == null)
                {
                        x += direction.x * (speed * tempTime);
                        y += direction.y * (speed * tempTime);

                        HXP.camera.x = x;
                        HXP.camera.y = y;
                }
                else
                {
                        x = tempHole.x + Math.cos(radian * speed) * diameter;
                        y = tempHole.y + Math.sin(radian * speed) * diameter;

                        angle += tempTime;
                        radian = angle * (Math.PI / 180);
                        if(angle >= 360) angle -= 360;

                        if(dist > diameter) dist -= 1;
                }
	}

	//checks for collision against various types
	private inline function checkCollision()
	{
	        collideEntity = collide("hole", x, y);
                var angleVec:Float;

                if(collideEntity != null)
                {
                        try
                        {
                                tempHole = cast(collideEntity, BlackHole);
                        }
                        catch (idk:Dynamic)
                        {
                                if(collideEntity.type == "bullet") HXP.scene.remove(this);
                        }

                        if(dist == 0) dist = Math.sqrt(Math.pow(tempHole.x - x, 2) + Math.pow(tempHole.y - y, 2));
                }
                else if (tempHole != null && collideEntity == null)
                {
                        direction.x = tempHole.x + Math.cos(radian * speed) * diameter - x;
                        direction.y = tempHole.y + Math.sin(radian * speed) * diameter - y;
                        tempHole = null;
                }
                else
                {
                        first = true;
                        angle = 0;
                }
	}

	private inline function handleInput()
	{
	}
	
	//cross method
	public override function update()
	{
		super.update();
		checkCollision();
		handleInput();
		move();
	}
}

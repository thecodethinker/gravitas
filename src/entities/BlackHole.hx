package entities;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.Graphic;
import com.haxepunk.Mask;
import com.haxepunk.math.Vector;
import com.haxepunk.masks.Circle;

class BlackHole extends Entity
{
	public var force:Float;
	public var direction:Vector;
	
	public function new(force:Float = 1, x:Float = 0, y:Float = 0, graphic:Graphic = null, mask:Mask = null)
	{
		super(x, y, graphic, mask);
		this.force = force;
		type = "hole";
		collidable = true;
		graphic.x = -width;
		graphic.y = -height;
		setHitbox(100, 100, 50, 50);
	}
}
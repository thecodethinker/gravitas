package entities;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Mask;
import com.haxepunk.Graphic;

class Enemy extends Entity
{
        private var mOffsetX:Float;
        private var mOffsetY:Float;
        private var mTempTime:Float;
        private var mAngle:Float;
        private var mRadAngle:Float;
        private var mMoveCooldown:Float;
        private var mMoveSin:Float;

        public function new(x:Float, y:Float, graphic:Graphic, mask:Mask)
        {
                super(x, y, graphic, mask);

                mOffsetX = x;
                mOffsetY = y;

                mTempTime = 0;
                mAngle = 0;
                mMoveSin = 10;
                mMoveCooldown = 0;
        }


        private inline function move()
        {
                if( y <= HXP.height / 8)
                {
                     y += 100 * mTempTime; 
                }
        }

        private inline function moveSin()
        {
                mAngle += mTempTime;
                mRadAngle = mAngle * (Math.PI / 180);

                if(mAngle >= 360) mAngle -= 360;

                x = mOffsetX + Math.sin(mRadAngle * 100) * 100; 
                y += 100 *  mTempTime;
        }

        public override function update()
        {
                super.update();
                mTempTime = HXP.elapsed;
                mMoveCooldown += mTempTime;

                if(mMoveCooldown >= mMoveSin)
                {
                        moveSin();
                }
                else
                {
                        move();
                }
        }
}

package;

import com.haxepunk.Engine;
import com.haxepunk.HXP;
import worlds.Level;

class Main extends Engine
{
	override public function init()
	{
#if debug
		HXP.console.enable();
#end
	        HXP.scene = new Level();
	}

	public static function main()
	{
		new Main();
	}
}
